$(document).ready(function(){
	$('.popup').find('.close').click(function(){
		$('.popup').fadeOut(600);
	})
	$('.forgot_pass a').click(function(){
		$('.popup').fadeOut(300)
	})
	$('.registration a').click(function(){
		$('.popup').fadeOut(300)
	})
	$('.popup a.back').click(function(){
		$('.popup').fadeOut(600)
	})
	$('.remember_me label').click(function(){
		$(this).toggleClass('active');
	})
	$('.history tbody').find('tr:gt(7)').hide();
	$('.private_cabinet .history').find('.show_all').click(function(){
		$('.history tbody').find('tr:gt(7)').show('slow');
	});
	$('form.profile .profile_changes').find('.btn').click(function(e){
		$(".inputs input").prop('disabled', false).addClass('active');
		e.preventDefault()
	})
	$('.cart_wrap tbody').find('.delete a').click(function(e){
		$(this).parents('tr').fadeOut();
		e.preventDefault()
	})
    var sbula = $(".catalog_page .filter").find('a');
    sbula.click(function(e) {
        sbula.removeClass("active").not(sbula).add(this).toggleClass("active");
        e.preventDefault();
    	});
    var method_preparation = $(".method_preparation .radio_label");
    method_preparation.click(function() {
        method_preparation.removeClass("active").not(method_preparation).add(this).toggleClass("active");
    	});
    var payment_methods = $(".payment_methods .radio_label");
    payment_methods.click(function() {
        payment_methods.removeClass("active").not(payment_methods).add(this).toggleClass("active");
    	});
    setTimeout(function() {
      var mainDivs = $(".column");
      var maxHeight = 0;
      for (var i = 0; i < mainDivs.length; ++i) {
        if (maxHeight < $(mainDivs[i]).height()) {
          maxHeight = $(mainDivs[i]).height();
        }
      }
      for (var i = 0; i < mainDivs.length; ++i) {
        $(mainDivs[i]).height(maxHeight);
      }
    },0);
	PopUpHide();
        $('.popup').click(function(){
            $(this).fadeOut(600);
        });
        $('.popup-content').click(function(e){
            e.stopPropagation();
        });
        $('ul.tabs').delegate('li:not(.current)', 'click', function() {
		$(this).addClass('current').siblings().removeClass('current')
			.parents('div.section').find('div.box').hide().eq($(this).index()).fadeIn(150);
	});
});
function PopUpShowEnter(){
          $("#enter").fadeIn(600)
        };
function PopUpShowRecoveryPass(){
          $("#recovery").fadeIn(600)
        };
function PopUpShowRegistration(){
          $("#registration").fadeIn(600)
        };
function PopUpShowConfirmOrder(){
          $("#confirm_order").fadeIn(600)
        };
function PopUpShowPromotion1(){
          $("#promotion1").fadeIn(600)
        };
function PopUpShowPromotion2(){
          $("#promotion2").fadeIn(600)
        };
		function PopUpHide(){
            enter = $("#enter");
            registration = $("#registration");
            recovery = $("#recovery");
            confirm_order = $("#confirm_order");
            promotion1 = $("#promotion1");
            promotion2 = $("#promotion2");
        };
function PopUpFade(){
            a = $(".popup");
            a.fadeOut();
        };